#!/usr/bin/env python
import numpy as np
import pandas as pd
from sklearn.cluster import KMeans

def main():
    df = pd.read_csv("data/02-data.csv")

    lng, lat = map(lambda x : x.dropna(), (df.Lng, df.Lat))
    data = list(zip(lng, lat))
    kmeans = KMeans(n_clusters=32)
    kmeans.fit(data)

    c = kmeans.cluster_centers_

    i = kmeans.inertia_
    n = kmeans.n_iter_
    f = kmeans.n_features_in_

    print("(inertia, iterations, features) = (%f, %d, %d)" % (i, n, f))
    print("Cluster centers: ")
    print(c)

if __name__ == '__main__':
    main()

