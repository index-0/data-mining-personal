#!/usr/bin/env python
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import statistics as s
from mpl_toolkits.basemap import Basemap

def main():
    df = pd.read_csv("data/02-data.csv")

    lat, lng = map(lambda x : x.dropna(), (df.Lat, df.Lng))
    m, b = s.linear_regression(lat, lng)

    print("y = mx + b: lng = %s lng + %s" % (str(m), str(b)))

    wm = Basemap(projection='ortho', lat_0=23.634501, lon_0=-102.552784, resolution='l')
    wm.drawcoastlines()
    wm.fillcontinents(color='coral',lake_color='aqua')
    wm.drawparallels(np.arange(-90.,120.,30.))
    wm.drawmeridians(np.arange(0.,420.,60.))
    wm.drawmapboundary(fill_color='aqua')

    x, y = wm(lng, lat)
    plt.scatter(x, y, c='#900925', s=0.5, alpha = 0.5)

    x = np.arange(min(lng), max(lng), 0.1)
    y = [m * i + b for i in x]
    x, y = wm(x, y)
    plt.plot(x, y)

    plt.xlabel('Longitud')
    plt.ylabel('Latitud')
    plt.show()

if __name__ == '__main__':
    main()

