#!/usr/bin/env python
import pandas as pd
from collections import Counter
from itertools import chain

def main():
    df = pd.read_csv("data/02-data.csv")
    a = list(chain.from_iterable(list(map(lambda x : x.lower().split(), df['Asentamiento']))))

    print("Asentamientos:")
    for i in Counter(a).most_common(25):
        print(i)

if __name__ == '__main__':
    main()
