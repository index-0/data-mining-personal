#!/usr/bin/env python
import csv
import pandas as pd
import statistics as s

def pstats(n, x):
    print("%s: (mean, median, mode, stdev) = (%s, %s, %s, %s)" %
          (n, str(s.mean(x)), str(s.median(x)), str(s.mode(x)), str(s.stdev(x))))

def main():
    df = pd.read_csv("data/02-data.csv")
    lat, lng, lda = map(lambda x : x.dropna(), (df.Lat, df.Lng, df.Lada))

    pstats("lat", lat)
    pstats("lng", lng)
    pstats("lda", lda)

if __name__ == '__main__':
    main()

