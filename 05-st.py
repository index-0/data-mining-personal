#!/usr/bin/env python
import numpy as np
import pandas as pd
import statistics as s

def main():
    df = pd.read_csv("data/02-data.csv")

    lat, lng = map(lambda x : x.dropna(), (df.Lat, df.Lng))
    cov, cor = s.covariance(lat, lng), s.correlation(lat, lng)

    print("cov(lat, lng) = %s" % cov)
    print("cor(lat, lng) = %s" % cor)

if __name__ == '__main__':
    main()

