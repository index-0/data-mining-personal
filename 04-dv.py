#!/usr/bin/env python
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from mpl_toolkits.basemap import Basemap

def main():
    df = pd.read_csv("data/02-data.csv")
    lat, lng = map(lambda x : x.dropna(), (df.Lat, df.Lng))
    m = Basemap(projection='ortho', lat_0=23.634501, lon_0=-102.552784, resolution='l')
    m.drawcoastlines()
    m.fillcontinents(color='coral',lake_color='aqua')
    m.drawparallels(np.arange(-90.,120.,30.))
    m.drawmeridians(np.arange(0.,420.,60.))
    m.drawmapboundary(fill_color='aqua')
    x,y = m(lng,lat)
    plt.scatter(x, y, c='#900925', s=0.5, alpha = 0.5)
    plt.show()

if __name__ == '__main__':
    main()
