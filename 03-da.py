#!/usr/bin/env python
import pandas as pd

def main():
    df = pd.read_csv("data/02-data.csv")
    print(df.filter(['Estado', 'Municipio', 'Asentamiento', 'Lada']))

if __name__ == '__main__':
    main()
