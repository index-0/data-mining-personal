#!/usr/bin/env python
import csv
import re
import requests
import lxml.html as lh

import util

surl = 'https://micodigopostal.org'

def get_doc(url: str):
    return lh.fromstring(requests.get(url).content)

def scrap_d0(doc):
    path = "//tbody/tr/td[%d]/a"
    return [(i.text_content(), i.get('href')) for i in doc.xpath(path % 1) + doc.xpath(path % 2)]

def scrap_d1(doc):
    path = "//tbody/tr/td/a"
    return [(i.text_content(), i.get('href')) for i in doc.xpath(path)]

def scrap_d2(doc):
    path = "//table/tbody/tr/td[not(@class)]/.."
    return [([a.text_content(), b.text_content(), c.text_content(), f.text_content()], a[0].get('href'))
            for (a, b, c, _, _, f, _) in doc.xpath(path)]

def scrap_d3(doc):
    path = "//script[@type and contains(text(), 'openstreetmap')]/text()"
    rexp = "(?<=(lat|lng)=)([+-]?\d+(?:\.\d+)?)(?=;)"
    if (not (elems := doc.xpath(path))):
        return []
    return [i[1] for i in re.findall(rexp, elems[0])]

def main():
    with open('00-mcp.csv', 'w') as f:
        write = csv.writer(f)
        write.writerow(['Estado', 'Municipio', 'Asentamiento', 'Tipo de Asentamiento', 'Código Postal', 'Zona', 'lat', 'lng'])
        states = scrap_d0(get_doc(surl))
        for i in range(0, len(states)):
            print("\nProcessing: " + states[i][0])
            counties = scrap_d1(get_doc(surl + states[i][1]))
            for j in range(0, nc := len(counties)):
                settlements = scrap_d2(get_doc(surl + counties[j][1]))
                for k in range(0, ns := len(settlements)):
                    coordinates = scrap_d3(get_doc(surl + settlements[k][1]))
                    write.writerow([states[i][0], counties[j][0]] + settlements[k][0] + coordinates)
                    util.progbar(((k / ns) * (1 / nc) + (j / nc)) * 100, 100) # the progress is not linear.

if __name__ == '__main__':
    main()
