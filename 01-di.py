#!/usr/bin/env python

def pp_data(data):
        for i in data[:10]:
            print(i)
        for i in range(0, 5):
            print("\t\t\t.")
        for i in data[-10:]:
            print(i)

def main():
    pp_data(util.csv_import("data/00-clm.csv"))
    pp_data(util.csv_import("data/00-mcp.csv"))

if __name__ == '__main__':
    main()

