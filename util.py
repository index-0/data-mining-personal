import csv
import sys

def csv_import(path):
    with open(path, 'r') as f:
        return list(csv.reader(f))

def progbar(p, n):
    if (n <= 0):
        return

    b = 100
    g = int(round((b * (p + 1)) / float(n + 1)))
    bar = '=' * g + '-' * (b - g)

    sys.stdout.write('[%s] %s%%\r' % (bar, g))
    sys.stdout.flush()
