#!/usr/bin/env python
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from sklearn.cluster import KMeans
from mpl_toolkits.basemap import Basemap

def main():
    df = pd.read_csv("data/02-data.csv")

    lng, lat = map(lambda x : x.dropna(), (df.Lng, df.Lat))
    data = list(zip(lng, lat))
    kmeans = KMeans(n_clusters=32)
    kmeans.fit(data)

    wm = Basemap(projection='ortho', lat_0=23.634501, lon_0=-102.552784, resolution='l')
    wm.drawcoastlines()
    wm.fillcontinents(color='coral',lake_color='aqua')
    wm.drawparallels(np.arange(-90.,120.,30.))
    wm.drawmeridians(np.arange(0.,420.,60.))
    wm.drawmapboundary(fill_color='aqua')

    x, y = wm(lng, lat)
    plt.scatter(x, y, c=kmeans.labels_, s=0.5, alpha = 0.5)
    plt.xlabel('Longitud')
    plt.ylabel('Latitud')
    plt.show()

if __name__ == '__main__':
    main()

