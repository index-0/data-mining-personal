#!/usr/bin/env python
import csv
import lxml.html as lh
import re
import requests
import sys

import util

surl = 'https://www.clavelada.com.mx'

def get_doc(url: str):
    return lh.fromstring(requests.get(url).content)

def nelem(doc):
    path = "//div[@class='paginacion']/div[last()]/a/@href"
    return int(re.findall("/(\d+)/", doc.xpath(path)[0])[0])

def scrap_d0(doc):
    path = "//div[@class='ctrLink']/a/@href"
    return doc.xpath(path)

def scrap_d1(doc):
    path = "//article/div[@class='divDatos']"
    return [scrap_d1h(list(e)) for e in doc.xpath(path)]

def scrap_d1h(c):
    a = c[2].text_content().split(", ", 1)
    a.reverse()
    a.append(c[0].text_content())
    a += re.findall("(?<=Clave Lada ).*", c[3].text_content())
    return a

def main():
    with open('00-clm.csv', 'w') as f:
        write = csv.writer(f)
        write.writerow(['Estado', 'Municipio', 'Asentamiento', 'Lada'])

        states = scrap_d0(get_doc(surl + "/mexico/ladas-por-estado/"))
        for i in range(0, len(states)):
            print('\nProcessing url: ' + states[i])
            n = nelem(get_doc(states[i]))
            for j in range(0, n + 1, 50):
                write.writerows(scrap_d1(get_doc(states[i] + str(j) + "/")))
                util.progbar(j, n)

if __name__ == '__main__':
    main()

