# Data Mining

## Index
00. Data Adquisition
01. Data Importing
02. Data Cleaning and Data Statistics
03. Data Analysis
04. Data Visualization
05. Statistic Test
06. Linear Models
07. Forecasting
08. Data Classification
09. Data Clustering
10. Text Analysis

## P00 - Data Adquisition
I obtained the data by scrapping two sources; [Mi Codigo Postal](https://www.micodigopostal.org) and [Claves Lada de México](https://www.clavelada.com.mx/). The first is a website to search for postal codes given the name of a municipality or to see a list of settlements by state and county name. The last is similar, but for searching Lada codes.

## P01 - Data Importing
I imported the obtained data from the previous practice and printed the five first and last rows.

## P02 - Data Cleaning and Data Statistics
Since I got the data from two slightly similar sources, the cleanup consisted of adding empty fields to the missing data and unifying both sources. I then computed a couple of simple statistical calculations from the data set; mean, median, mode, and standard deviation.

## P03 - Data Analysis
I used the Pandas library for importing and analyzing the data set.

## P04 - Data Visualization
For visualizing the data, I used the Basemap library from Matplotlib to plot the coordinates of each of the settlements on a world map.

![Data Visualization](img/04-dv.png)

## P05 - Statistic Test
I performed some basic statistical measures; correlation and covariance. And it was found that there's some correlation between latitude and longitude. And that the tendency of the linear relationship of both variables showed an opposite behavior.

## P06 - Linear Models
I computed the linear regression of the longitude and latitude, and the resulting line was plotted over the world map from practice 4.

![Linear Models](img/06-lm.png)

## P07 - Forecasting

## P08 - Data Classification
I computed the K-nearest neighbors for the latitude and longitude and printed the centers of each resulting K cluster.

## P09 - Data Clustering
I plotted the map from practice four, but now with colors applied to each point according to the group they belong to.

![Data Clustering](img/09-dc.png)

## P10 - Text Analysis
I computed the count of the 25 most common words for settlement's names and printed them.

