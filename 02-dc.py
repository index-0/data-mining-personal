#!/usr/bin/env python
import csv
import difflib

import util

def csv_write_rows(path: str, first, rows):
    with open(path, 'w') as f:
        write = csv.writer(f)
        write.writerow(first)
        write.writerows(rows)

def ctree(x, o):
    a, b, c = [], [], []
    ca, cb = x[0][0], x[0][1]
    for i in range(0, len(x) - 1):
        c.append(x[i][o:])
        if (cb != x[i + 1][1]):
            b.append((cb, c))
            cb, c = x[i + 1][1], []
            if (ca != x[i + 1][0]):
                a.append((ca, b))
                ca, b = x[i + 1][0], []

    c.append(x[i + 1][o:])
    b.append((cb, c))
    a.append((ca, b))
    return a

# TODO: this function sucks a lot. posible improvements:
# * edit get_close_matches from difflib to return the indexes instead,
# * convert the strings involved in search operations to binary trees,
# * or just don't, please.
def index(x, y):
    indxfn, indxnf = [], []
    for i in range(0, len(x)):
        la = list(map(lambda x: x[0], y[i][1]))
        for j in range(0, len(x[i][1])):
            a = difflib.get_close_matches(x[i][1][j][0], la, n=1, cutoff=0.6)
            if (not a):
                indxnf.append(((i, j, 0), len(x[i][1][j][1])))
                continue
            ij = la.index(a[0])
            lb = list(map(lambda x: x[0], y[i][1][ij][1]))
            for k in range(0, len(x[i][1][j][1])):
                b = difflib.get_close_matches(x[i][1][j][1][k][0], lb, n=1, cutoff=0.6)
                if (not b):
                    indxnf.append(((i, j, k), 1))
                    continue
                ik = lb.index(b[0])
                indxfn.append((i, (j, k), (ij, ik)))
    return (indxfn, indxnf)

def main():
    mcp = util.csv_import("data/00-mcp.csv")
    clm = util.csv_import("data/00-clm.csv")

    mcps = sorted(mcp[1:], key = lambda x: (x[0], x[1], x[2]))
    clms = sorted(clm[1:], key = lambda x: (x[0], x[1], x[2]))

    for i in range(0, len(mcps)):
        if (len(mcps[i]) == 6):
            mcps[i] += ['', '']

    csv_write_rows("data/02-mcp.csv", mcp[0], mcps)
    csv_write_rows("data/02-clm.csv", clm[0], clms)

    mcpt = ctree(mcps, 2)
    clmt = ctree(clms, 2)

    x = index(mcpt, clmt)
    fndata = []
    for (i, j, k) in x[0]:
        fndata.append([mcpt[i][0]] +
                      [mcpt[i][1][j[0]][0]] +
                       mcpt[i][1][j[0]][1][j[1]] +
                      [clmt[i][1][k[0]][1][k[1]][1]])

    nfdata = []
    for (t, n) in x[1]:
        for i in range(0, n):
            nfdata.append([mcpt[t[0]][0]] +
                          [mcpt[t[0]][1][t[1]][0]] +
                          mcpt[t[0]][1][t[1]][1][t[2]] +
                          [''])

    frow = ["Estado", "Municipio", "Asentamiento", "Tipo de Asentamiento", "Código Postal", "Zona", "Lat", "Lng", "Lada"]
    csv_write_rows("data/02-fndata.csv", frow, fndata)
    csv_write_rows("data/02-nfdata.csv", frow, nfdata)

    data = sorted(fndata + nfdata, key = lambda x: (x[0], x[1], x[2]))
    csv_write_rows("data/02-data.csv", frow, data)

if __name__ == '__main__':
    main()

